**Table of contents**

[TOC]

# 1. HE-CODECO NetMA sub-component netma-nsm-mon

Netma-nsm-mon is a sub-component of the HE-CODECO NetMA that focuses on probing the CODECO selected network metrics [(refer to the CODECO Deliverable D9, Annex I)](https://zenodo.org/records/8143860). Examples of networking metrics being collected from an underlay network perspective are latency, bandwidth, link energy.

This specific adaptation covers the following networking metrics:

- bandwidth/throughput
- latency
- packet loss
- jitter
- energy
- link failure

Its usage scenarios include:

- Real-time Network Monitoring: Continuously monitoring network metrics.
- On-Demand Network Measurement: Initiating network performance measurements based on external triggers or user requests.

Two probing mechanisms are provided, splitted by the folders in the repository (socket-probe and netperf-probe). Further information is provided in their respective README files.

![NetMa_architecture](./NetMa_architecture.png)

# 2. Quick Start

Steps for getting a demo to run on a cluster.

## 2.1 Preconditions

- Kubectl
- A cluster with no less than 2 nodes

## 2.2 Copy required scripts to local environment

- One way is to clone the netma-nsm-mon repository

```
cd <nsm-mon-directory>
git clone https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/network-management-and-adaptation-netma/network-state-management.git
```

- Deploy probes
  - Option A (socket-probe)
  ```
  cd socket-probe
  kubectl apply -f daemonset.yaml
  ```
  - Option B (netperf-probe)
  ```
  cd netperf-probe/k8s-netperf
  kubectl apply -f k8s-netperf.yaml
  ```

# 3. Licensing

Apache 2.0

# Credits

- socket-probe is developed and maintained by Universidad Politécnica de Madrid (UPM)
- netperf-probe, and respective port to python and adaptations to HE-CODECO NetMA are being developed by fortiss GmbH, Hongyu Guo.