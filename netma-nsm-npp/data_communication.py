import time

import pika
import requests
from kafka import KafkaProducer
from prometheus_client import Gauge, start_http_server

import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')


def kafka_connection(kafka_server):
	producer = KafkaProducer(bootstrap_servers=kafka_server+':9092')

	return producer


def kafka_send(producer, json_data, topic):
	producer.send(topic, value=json_data.encode('utf-8'))


def rabbit_connection(rabbit_server):
	connection = pika.BlockingConnection(pika.ConnectionParameters(rabbit_server))
	channel = connection.channel()

	return channel, connection


def rabbit_send(channel, connection, json_data, queue):
	channel.basic_publish(exchange='', routing_key=queue, body=json_data)

	connection.close()


def api_send(json_data, api_url):
	headers = {'Content-Type': 'application/json'}
	response = requests.post(api_url, data=json_data, headers=headers)

	if response.status_code == 200:
		print("Data sent successfully")
	else:
		print(f"Failed to send data. Status code: {response.status_code}")
		print(response.text)


def prometheus_connection(port, _metric_names):
	metric_gauges = {}

	help_strings = {
		'throughput': 'Network Metric for throughput (Mbps)',
		'packet_loss': 'Network Metric for packet loss (percentage)',
		'latency': 'Network Metric for latency (ms)',
		'jitter': 'Network Metric for jitter (ms)',
		'congestion': 'Network Metric for network congestion (percentage)',
		'link_energy': 'Energy consumption (W)',
		'link_failure': 'Link failure (0=okay; 1=not okay)'
	}

	for name in _metric_names:
		metric_gauges[name] = Gauge(
			f'network_metric_{name}',
			help_strings[name],
			['current_node', 'remote_node', 'remote_ip']
		)

	start_http_server(port)

	return metric_gauges


def prometheus_send(_metrics, _data, _ip, _current_node, _remote_node):

	for metric_name, metric_value in _data.items():
		if metric_name in _metrics:
			_metrics[metric_name].labels(
				current_node=_current_node, remote_node=_remote_node, remote_ip=_ip).set(metric_value['value'])


def flask_start(app, port):
	app.run(host='0.0.0.0', port=port)