**Table of contents**

[TOC]

# 1. HE-CODECO NetMA sub-component netma-nsm-mon

Netma-nsm-mon is a sub-component of the HE-CODECO NetMA that focuses on probing the CODECO selected network metrics [(refer to the CODECO Deliverable D9, Annex I)](https://zenodo.org/records/8143860). Examples of networking metrics being collected from an underlay network perspective are latency, bandwidth, link energy.

Netma-nsm-mom has been based on the existing open-source code of the [Kubernetes netperf](https://github.com/leannetworking/k8s-netperf) licensed under Apache 2.0.

This specific adaptation covers the following networking metrics:

- bandwidth
- latency
- packet loss
- jitter
- network interface statistics

Provides the following: the probe is designed to measure various network metrics, including bandwidth, latency, packet loss rate, node degree, link failure, and link energy.

Its usage scenarios include:

- Real-time Network Monitoring: Continuously monitoring network metrics.
- On-Demand Network Measurement: Initiating network performance measurements based on external triggers or user requests.

![NetMa_architecture](./NetMa_architecture.png)



# 2. Quick Start

Steps for getting a demo to run on a cluster.

## 2.1 Preconditions

- Kubectl
- A cluster with no less than 2 nodes

## 2.2 Copy required scripts to local environment

- One way is to clone the netma-nsm-mon repository

```
cd <nsm-mon-directory>
git clone -b guo https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/network-management-and-adaptation-netma/network-state-management.git
```

## 2.3 Test

### 2.3.1 Removing Taints from Master Nodes

**Viewing All Nodes and Their Taints**: You can get an overview of all nodes using the command `kubectl get nodes -o wide`. However, to view specific taint information, you need to use the command `kubectl describe node [node name]` or get more detailed output with the `-o json` parameter.

```python
kubectl get node [node name] -o json | jq '.spec.taints'
# or
kubectl describe node [node name]
```

To remove a taint from a node, you can use the `kubectl taint` command. The basic syntax for removing a taint is as follows:

```bash
kubectl taint nodes <node-name> <taint-key>-
```

An example：

```bash
kubectl taint nodes my-cluster-control-plane node-role.kubernetes.io/control-plane-
```

### 2.3.2 Deploying netperf as Daemon Pods

The netperf component, based on the [k8s-netperf](https://github.com/leannetworking/k8s-netperf) open-source project, has been enhanced in our Dockerfile to ensure compatibility across both arm64 and amd64 architectures.

To deploy, follow these steps:

```
cd k8s-netperf
kubectl apply -f k8s-netperf.yaml
```

### 2.3.3 Running the Code

You can specify the network interface to monitor link energy using tcpdump via the `--interface` option. For example:

```
sudo python3 network_probe.py --interface wlp59s0
```

This script will generate a `netperfMetrics.txt` file, which stores six network metrics: latency, bandwidth, link energy, node degree, link failure, and packet loss. These metrics are then converted into a ConfigMap. A ConfigMap named `netperf-metrics` will be created in the default namespace.

An example of the ConfigMap output is as follows:

```
codeco@master:~/network-state-management/Monitoring$ kubectl get configmap netperf-metrics -o yaml
apiVersion: v1
data:
  iPerf.bw.Gbits.Sec.origin.master.destination.working0: 22.0 Mbits/sec
  iPerf.bw.Gbits.Sec.origin.master.destination.working1: 15.8 Mbits/sec
  iPerf.bw.Gbits.Sec.origin.master.destination.working2: 16.3 Mbits/sec
  iPerf.loss.origin.master.destination.working0: 0%
  iPerf.loss.origin.master.destination.working1: 0%
  iPerf.loss.origin.master.destination.working2: 0%
  link.energy.consumption.from.master.to.working0.during.2: 442943.50.microwatts
  link.energy.consumption.from.master.to.working1.during.2: 441275.90.microwatts
  link.energy.consumption.from.master.to.working2.during.2: 365095.60.microwatts
  link.failure.from.master.to.working0.during.1: 0.times
  link.failure.from.master.to.working1.during.1: 0.times
  link.failure.from.master.to.working2.during.1: 0.times
  netperf.p90.latency.milliseconds.origin.master.destination.working0: "6900"
  netperf.p90.latency.milliseconds.origin.master.destination.working1: "6250"
  netperf.p90.latency.milliseconds.origin.master.destination.working2: "6400"
  node.degree.of.master: "3"
kind: ConfigMap
metadata:
  creationTimestamp: "2024-06-17T13:46:58Z"
  name: netperf-metrics
  namespace: default
  resourceVersion: "67367738"
  uid: c5ea0629-bf9b-4d20-8ab5-070c7c3adfe3
```

Please note that the logic for calculating link energy still requires further testing. The actual link energy may differ from the obtained values.

# 3. Licensing

Apache 2.0

# 4. Contributing
Create your own branch and start updates - request merges when necessary

# Credits
Port to python and adaptations to HE-CODECO NetMA are being developed by fortiss GmbH, Hongyu Guo.

