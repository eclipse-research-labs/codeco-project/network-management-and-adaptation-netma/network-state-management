# Copyright (c) 2024 fortiss GmbH
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Hongyu Guo, fortiss - Author

import json
import subprocess
import re
import sys
import os

filename = 'netperfMetrics.txt'

# Clear or create the file
with open(filename, 'w') as fh:
    pass

# Example parameters
numberOfLocalhostMeasurements = 20
iperfTime = 2
netperfRequestPacketSize = 32
netperfResponsePacketSize = 1024

# Dictionaries to store data
nodes = {}
pods = {}
services = {}

# Simple IP address regex
IPregexp = r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'

# check baseline flag
nobaseline = "--nobaseline" in sys.argv
print("No Baseline flag is:", nobaseline)

# Function to execute shell commands and return output
def run_command(command):
    result = subprocess.run(command, shell=True, text=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return result.stdout.strip()

# Function to get Kubernetes info: nodes, pods, services
def get_kubernetes_info():
    # Get all nodes
    all_nodes = run_command("kubectl get nodes -o name").split('\n')
    for node in all_nodes:
        node_name = node.split('/')[1]
        print(f"Node: {node_name}")

        node_info = run_command(f"kubectl describe node {node_name}")
        ip_address_match = re.search(r'InternalIP:\s+(\d+\.\d+\.\d+\.\d+)', node_info)
        pod_cidr_match = re.search(r'PodCIDR:\s+(\d+\.\d+\.\d+\.\d+/\d+)', node_info)

        nodes[node_name] = {
            'pods': [],
            'IPaddress': ip_address_match.group(1) if ip_address_match else None,
            'PodCIDR': pod_cidr_match.group(1) if pod_cidr_match else None,
            'HostNetPerf' : []
        }

    # Get all pods
    all_pods = run_command("kubectl get pods -o=custom-columns=NAME:.metadata.name,NAMESPACE:.metadata.namespace | grep netperf").split('\n')
    for pod in all_pods:
        name, namespace = pod.split()
        print(f"Pod: {name} in {namespace}")

        pod_info = run_command(f"kubectl describe pod {name} --namespace={namespace}")
        ip_address_match = re.search(r'IP:\s+(\d+\.\d+\.\d+\.\d+)', pod_info)
        node_info_match = re.search(r'Node:\s+(.+?)/(\d+\.\d+\.\d+\.\d+)', pod_info)

        pod_dict = {
            'namespace': namespace,
            'IPaddress': ip_address_match.group(1) if ip_address_match else None,
            'NodeName': node_info_match.group(1) if node_info_match else None,
            'NodeIP': node_info_match.group(2) if node_info_match else None,
        }

        if pod_dict['IPaddress'] == pod_dict['NodeIP']:
            nodes[pod_dict['NodeName']]['HostNetPerf'].append(name)
        else:
            nodes[pod_dict['NodeName']]['pods'].append(name)
            pods[name] = pod_dict

    # Get all services
    all_services = run_command("kubectl get services --all-namespaces -o wide | grep netperf").split('\n')
    for service in all_services:
        namespace, name, type_, cluster_ip, external_ip, port, age, selector = service.split()[:8]
        print(f"Service: {name} in {namespace} IP={cluster_ip} {external_ip} {selector}")

        services[name] = {
            'namespace': namespace,
            'type': type_,
            'clusterIP': cluster_ip,
            'externalIP': external_ip,
            'selector': selector,
            'podBackends': []
        }

        # Get pods for service
        pod_backends = run_command(f"kubectl get pods -l {selector} --all-namespaces -o name").split('\n')
        services[name]['podBackends'] = [pod.split('/')[1] for pod in pod_backends]

    # Get ports for the services
    json_services = json.loads(run_command("kubectl get services --all-namespaces -o json"))
    for svc in json_services['items']:
        if svc['metadata']['name'] in services:
            services[svc['metadata']['name']]['ports'] = svc['spec']['ports']

    return nodes, pods, services

# function calls
nodes, pods, services = get_kubernetes_info()
print("nodes is: /n")
print(nodes)
print("pods is: /n")
print(pods)
print("services is: /n")
print(services)

# Check if there are less than 2 nodes, then exit
if len(nodes) < 2:
    print("Exiting because there are less than 2 nodes.")
    exit()

# Note: Implement specific network testing functions such as runIperf, runNetperf, runFortio based on your environment's specifics.
def run_iperf(pod, target_ip, iperf_time):
    pod_name = pod[0]
    command = f"kubectl exec -it {pod_name} -- iperf -c {target_ip} -i 1 -t {iperf_time}"
    print(f"running command: {command}", file=sys.stderr)
    stdout = run_command(command)
    last_line = stdout.strip().split('\n')[-1]
    print("last line is : ", last_line)
    match = re.search(r'(\d+\.\d+ [GM]bits/sec)', last_line)
    # Extract the bandwidth information
    if match:
        return match.group(1)
    else:
        return "Bandwidth information not found"
    
def run_iperf_udp(pod, target_ip, iperf_time):
    pod_name = pod[0]
    command = f"kubectl exec -it {pod_name} -- iperf -c {target_ip} -u -i 1 -t {iperf_time}"
    print(f"running command: {command}", file=sys.stderr)
    stdout = run_command(command)
    last_line = stdout.strip().split('\n')[-1]
    print("last line is : ", last_line)
    match = re.search(r'(\d+)/\s*(\d+)\s*\((\d+)%\)', last_line)
    # Extract the bandwidth information
    if match:
        return match.group(3) + '%'
    else:
        return "No packet loss information found."

def run_netperf(pod, server_ip, time, test_type, output_format):
    pod_name = pod[0]
    command = f"kubectl exec -it {pod_name} -- netperf -H {server_ip} -l {time} -P 1 -t {test_type} -- -r 32,1024 -o {output_format}"
    #  Execute the command and capture the output.
    stdout = run_command(command)
    print(f"stdout is: {stdout}")
    print(f"running command: {command}", file=sys.stderr)
    # return the last line output
    last_line = stdout.strip().split('\n')[-1]
    print(last_line)
    return last_line

def write_to_file(filename, data):
    with open(filename, 'a') as file:
        file.write(data + '\n')

def create_or_update_configmap(configmap_name, namespace, file_path):
    """Create or update a Kubernetes ConfigMap based on an environment file."""
    # Check if the ConfigMap exists
    command_check = f"kubectl get configmap {configmap_name} -n {namespace} --ignore-not-found"
    result_check = run_command(command_check)
    if result_check:
        # ConfigMap exists, proceed to update
        print(f"ConfigMap {configmap_name} exists, updating...")
        # To update, first delete the existing ConfigMap
        command_delete = f"kubectl delete configmap {configmap_name} -n {namespace}"
        run_command(command_delete)
        # Create a new ConfigMap with the updated file
        command_create = f"kubectl create configmap {configmap_name} --from-env-file=\"{file_path}\" -n {namespace}"
        run_command(command_create)
        print(f"ConfigMap {configmap_name} has been updated.")
    else:
        # ConfigMap does not exist, proceed to create
        print(f"ConfigMap {configmap_name} does not exist, creating...")
        command_create = f"kubectl create configmap {configmap_name} --from-env-file=\"{file_path}\" -n {namespace}"
        run_command(command_create)
        print(f"ConfigMap {configmap_name} has been created.")

def main():
    if os.path.exists(filename):
        os.remove(filename)
    nodes, netperf_pods, netperf_services = get_kubernetes_info()
    # run netperf for latency
    for n1 in nodes:
        for n2 in nodes:
            if n1 != n2 :
                pod_name = nodes[n1]['HostNetPerf']
                target_ip = nodes[n2]['IPaddress']
                netperf_RR = run_netperf(pod_name, target_ip, iperfTime, 'TCP_RR', 'P50_LATENCY,P90_LATENCY,P99_LATENCY,THROUGHPUT,THROUGHPUT_UNITS')
                print(f"InterNode NetPerf TCP_RR test between Node {n1} and Node {n2}: {netperf_RR}")
                # Splitting the result into four parts
                sc1, sc2, sc3, sc4, sc5 = netperf_RR.split(',') if netperf_RR else (None, None, None, None)
                write_to_file(filename, f"netperf.p90.latency.milliseconds.origin.{n1}.destination.{n2}={sc2}\n")
    
    # run iperf for bandwidth and packet loss
    for n1 in nodes:
        for n2 in nodes:
            if n1 != n2 :
                pod_name = nodes[n1]['HostNetPerf']
                target_ip = nodes[n2]['IPaddress']
                bw = run_iperf(pod_name, target_ip, iperfTime)
                print(f"InterNode iPerf Bandwidth test between Node {n1} and Node {n2}: {bw}")
                # Splitting the result into four parts
                write_to_file(filename, f"iPerf.bw.Gbits.Sec.origin.{n1}.destination.{n2}={bw}\n")
                packet_loss = run_iperf_udp(pod_name, target_ip, iperfTime)
                print(f"InterNode iPerf packet_loss test between Node {n1} and Node {n2}: {packet_loss}")
                # Splitting the result into four parts
                write_to_file(filename, f"iPerf.loss.origin.{n1}.destination.{n2}={packet_loss}\n")


    #node degree

    #node failure

    #link energy
    
                

    configmap_name = "netperf-metrics"
    namespace = "default"  
    file_path = "netperfMetrics.txt"  

    create_or_update_configmap(configmap_name, namespace, file_path)


if __name__ == "__main__":
    main()



