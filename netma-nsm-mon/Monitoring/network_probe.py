# Copyright (c) 2024 fortiss GmbH
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Hongyu Guo, fortiss - Author

import socket
import os
from datetime import datetime, timedelta
import time 
import subprocess
import re
import json
import sys
import asyncio

filename = 'netperfMetrics.txt'

# Clear or create the file
with open(filename, 'w') as fh:
    pass

# Example parameters
numberOfLocalhostMeasurements = 20
iperfTime = 2
netperfRequestPacketSize = 32
netperfResponsePacketSize = 1024

# Dictionaries to store data
nodes = {}
pods = {}
services = {}

# Simple IP address regex
IPregexp = r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'

# check baseline flag
nobaseline = "--nobaseline" in sys.argv
print("No Baseline flag is:", nobaseline)

# default interface
default_interface = "enp0s31f6"
interface = default_interface

# to check if command line has specified the interface
if "--interface" in sys.argv:
    interface_index = sys.argv.index("--interface") + 1
    if interface_index < len(sys.argv):
        interface = sys.argv[interface_index]

print("interface is:", interface)

# parameters for link energy
a_receive = 0.5
P0_receive = 356
a_send = 1.9
P0_send = 454
P0_idle = 843

# Function to execute shell commands and return output
def run_command(command):
    result = subprocess.run(command, shell=True, text=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    # Error Handling in Command Execution
    if result.returncode != 0:
        print(f"Error running command: {command}\nError: {result.stderr}")
    return result.stdout.strip()

# Function to get Kubernetes info: nodes, pods, services
def get_kubernetes_info():
    # Get all nodes
    all_nodes_info = run_command("kubectl get nodes -o json")
    
    # Convert the JSON output to a Python dictionary
    nodes_dict = json.loads(all_nodes_info)
    
    # Filter out nodes that do not have a 'NoSchedule' taint
    nodes_without_noschedule = []
    for node in nodes_dict["items"]:
        has_noschedule = any(taint.get("effect") == "NoSchedule" for taint in node.get("spec", {}).get("taints", []))
        if not has_noschedule:
            nodes_without_noschedule.append(node["metadata"]["name"])
    print(nodes_without_noschedule)

    for node_name in nodes_without_noschedule:
        print(f"Node: {node_name}")

        node_info = run_command(f"kubectl describe node {node_name}")
        ip_address_match = re.search(r'InternalIP:\s+(\d+\.\d+\.\d+\.\d+)', node_info)
        pod_cidr_match = re.search(r'PodCIDR:\s+(\d+\.\d+\.\d+\.\d+/\d+)', node_info)

        nodes[node_name] = {
            'pods': [],
            'IPaddress': ip_address_match.group(1) if ip_address_match else None,
            'PodCIDR': pod_cidr_match.group(1) if pod_cidr_match else None,
            'HostNetPerf' : []
        }

    # Get all pods
    all_pods = run_command("kubectl get pods -o=custom-columns=NAME:.metadata.name,NAMESPACE:.metadata.namespace | grep netperf").split('\n')
    for pod in all_pods:
        name, namespace = pod.split()
        print(f"Pod: {name} in {namespace}")

        pod_info = run_command(f"kubectl describe pod {name} --namespace={namespace}")
        ip_address_match = re.search(r'IP:\s+(\d+\.\d+\.\d+\.\d+)', pod_info)
        node_info_match = re.search(r'Node:\s+(.+?)/(\d+\.\d+\.\d+\.\d+)', pod_info)

        pod_dict = {
            'namespace': namespace,
            'IPaddress': ip_address_match.group(1) if ip_address_match else None,
            'NodeName': node_info_match.group(1) if node_info_match else None,
            'NodeIP': node_info_match.group(2) if node_info_match else None,
        }

        if pod_dict['IPaddress'] == pod_dict['NodeIP']:
            nodes[pod_dict['NodeName']]['HostNetPerf'].append(name)
        else:
            nodes[pod_dict['NodeName']]['pods'].append(name)
            pods[name] = pod_dict

    # Get all services
    all_services = run_command("kubectl get services --all-namespaces -o wide | grep netperf").split('\n')
    for service in all_services:
        namespace, name, type_, cluster_ip, external_ip, port, age, selector = service.split()[:8]
        print(f"Service: {name} in {namespace} IP={cluster_ip} {external_ip} {selector}")

        services[name] = {
            'namespace': namespace,
            'type': type_,
            'clusterIP': cluster_ip,
            'externalIP': external_ip,
            'selector': selector,
            'podBackends': []
        }

        # Get pods for service
        pod_backends = run_command(f"kubectl get pods -l {selector} --all-namespaces -o name").split('\n')
        services[name]['podBackends'] = [pod.split('/')[1] for pod in pod_backends]

    # Get ports for the services
    json_services = json.loads(run_command("kubectl get services --all-namespaces -o json"))
    for svc in json_services['items']:
        if svc['metadata']['name'] in services:
            services[svc['metadata']['name']]['ports'] = svc['spec']['ports']

    return nodes, pods, services

# function calls
nodes, pods, services = get_kubernetes_info()
print("nodes is: /n")
print(nodes)
print("pods is: /n")
print(pods)
print("services is: /n")
print(services)

link_address = {key: value['IPaddress'] for key, value in nodes.items()}

# Check if there are less than 2 nodes, then exit
if len(nodes) < 2:
    print("Exiting because there are less than 2 nodes.")
    exit()

# Note: Implement specific network testing functions such as runIperf, runNetperf, runFortio based on your environment's specifics.
def run_iperf(pod, target_ip, iperf_time):
    pod_name = pod[0]
    command = f"kubectl exec -it {pod_name} -- iperf -c {target_ip} -i 1 -t {iperf_time}"
    print(f"running command: {command}", file=sys.stderr)
    stdout = run_command(command)
    last_line = stdout.strip().split('\n')[-1]
    print("last line is : ", last_line)
    match = re.search(r'(\d+\.\d+ [GM]bits/sec)', last_line)
    # Extract the bandwidth information
    if match:
        return match.group(1)
    else:
        return "Bandwidth information not found"
    
def run_iperf_udp(pod, target_ip, iperf_time):
    pod_name = pod[0]
    command = f"kubectl exec -it {pod_name} -- iperf -c {target_ip} -u -i 1 -t {iperf_time}"
    print(f"running command: {command}", file=sys.stderr)
    stdout = run_command(command)
    last_line = stdout.strip().split('\n')[-1]
    print("last line is : ", last_line)
    match = re.search(r'(\d+)/\s*(\d+)\s*\((\d+)%\)', last_line)
    # Extract the bandwidth information
    if match:
        return match.group(3) + '%'
    else:
        return "No packet loss information found."

def run_netperf(pod, server_ip, time, test_type, output_format):
    pod_name = pod[0]
    command = f"kubectl exec -it {pod_name} -- netperf -H {server_ip} -l {time} -P 1 -t {test_type} -- -r 32,1024 -o {output_format}"
    #  Execute the command and capture the output.
    stdout = run_command(command)
    print(f"stdout is: {stdout}")
    print(f"running command: {command}", file=sys.stderr)
    # return the last line output
    last_line = stdout.strip().split('\n')[-1]
    print(last_line)
    return last_line

def write_to_file(filename, data):
    with open(filename, 'a') as file:
        file.write(data + '\n')

def create_or_update_configmap(configmap_name, namespace, file_path):
    """Create or update a Kubernetes ConfigMap based on an environment file."""
    # Check if the ConfigMap exists
    command_check = f"kubectl get configmap {configmap_name} -n {namespace} --ignore-not-found"
    result_check = run_command(command_check)
    if result_check:
        # ConfigMap exists, proceed to update
        print(f"ConfigMap {configmap_name} exists, updating...")
        # To update, first delete the existing ConfigMap
        command_delete = f"kubectl delete configmap {configmap_name} -n {namespace}"
        run_command(command_delete)
        # Create a new ConfigMap with the updated file
        command_create = f"kubectl create configmap {configmap_name} --from-env-file=\"{file_path}\" -n {namespace}"
        run_command(command_create)
        print(f"ConfigMap {configmap_name} has been updated.")
    else:
        # ConfigMap does not exist, proceed to create
        print(f"ConfigMap {configmap_name} does not exist, creating...")
        command_create = f"kubectl create configmap {configmap_name} --from-env-file=\"{file_path}\" -n {namespace}"
        run_command(command_create)
        print(f"ConfigMap {configmap_name} has been created.")

def get_local_ip():
    # Create a UDP connection (without needing to actually connect to an external server)
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # Connect to an external IP (without needing to actually send data)
        s.connect(("8.8.8.8", 80))
        ip = s.getsockname()[0]
    except Exception as e:
        ip = "127.0.0.1"
    finally:
        s.close()
    return ip

def get_node_name(local_ip):
    node_name = "unknown"
    for node, node_ip in link_address.items():
        if node_ip ==local_ip:
            node_name = node
    return node_name


def ping_node(address):
    response = os.system(f"ping -c 1 -w 1 {address} > /dev/null 2>&1")
    return response == 0

def node_degree(local_ip): 
    uNodeDegree = 0
    for name, address in link_address.items():
        if address != local_ip:
            if ping_node(address):
                uNodeDegree += 1
    return uNodeDegree


def link_failure(local_ip, duration_minutes=5):
    end_time = datetime.now() + timedelta(minutes=duration_minutes)
    failure_counts = {node: 0 for node in link_address}
    while datetime.now() < end_time:
        for node, address in link_address.items():
            if address != local_ip:
                if not ping_node(address):
                    failure_counts[node] += 1
        time.sleep(30)

    return failure_counts

def extract_ip(ip_port):
    parts = ip_port.split('.')
    # If the first part is not a digit, return only the first part
    if not parts[0].isdigit():
        return parts[0]
    # If the first part is a digit, return the first four parts (full IPv4 address)
    return '.'.join(parts[:4])

def calculate_energy(packet_size, a, P0):
    energy = (a * packet_size + P0) 
    return energy

def process_line(line, local_ip, link_energy, node_name):
    print(f"Processing line: {line.strip()}")
    packet_size_match = re.search(r'length (\d+)', line)
    if packet_size_match:
        print("match!!!")
        packet_size = int(packet_size_match.group(1))
        src_ip_match = re.search(r'IP ([\w\.\-]+)', line)
        print(f"src_ip_match: {src_ip_match}")
        dst_ip_match = re.search(r'> ([\w\.\-]+)', line)
        print(f"dst_ip_match: {dst_ip_match}")
        if src_ip_match and dst_ip_match and packet_size != 0:
            src_ip = extract_ip(src_ip_match.group(1))
            print(f"src_ip: {src_ip}")
            dst_ip = extract_ip(dst_ip_match.group(1))
            print(f"src_ip: {src_ip}, dst_ip: {dst_ip}, packet_size: {packet_size}")

            if (src_ip == local_ip or src_ip == node_name)  and dst_ip in link_address.values():
                energy = calculate_energy(packet_size, a_send, P0_send)
                link_energy[dst_ip] += energy
            elif (dst_ip == local_ip or dst_ip == node_name) and src_ip in link_address.values():
                energy = calculate_energy(packet_size, a_receive, P0_receive)
                link_energy[src_ip] += energy

def measure_node_degree(local_ip, node_name):
    uNodeDegree = node_degree(local_ip)
    print(f"Node degree of {node_name} node is {uNodeDegree}")
    write_to_file(filename, f"node.degree.of.{node_name}={uNodeDegree}\n")
    return node_name, uNodeDegree

async def measure_link_failures(local_ip, node_name, duration_minutes=5):
    failure_counts = link_failure(local_ip, duration_minutes)
    # Remove the item with the key equal to node_name from the dictionary
    if node_name in failure_counts:
        del failure_counts[node_name]

    # For the remaining keys, print "link failure time from source to node during 5 minutes is {value}" one by one.
    for node, value in failure_counts.items():
        print(f"Link failure time from {node_name} to {node} during {duration_minutes}min is {value}")
        write_to_file(filename, f"link.failure.from.{node_name}.to.{node}.during.{duration_minutes}={value}.times\n")
    return node_name, failure_counts

async def measure_link_energy(local_ip, node_name, duration_minutes=5, interface = interface):
    link_energy = {ip: 0 for ip in link_address.values() if ip != local_ip}
    filter_exp = " or ".join([f"src {ip} or dst {ip}" for ip in link_address.values() if ip != local_ip])
    output_file = os.path.join(os.getcwd(), "tcpdump_output.pcap")

    # remove output file if it already exits
    if os.path.exists(output_file):
        os.remove(output_file)

    tcpdump_cmd = ["tcpdump","-i", interface, "-n", filter_exp, "-w", output_file]

    print("tcpdump_cmd:", " ".join(tcpdump_cmd))
    process = subprocess.Popen(tcpdump_cmd)
    try:
        process.communicate(timeout=duration_minutes * 60)
    except subprocess.TimeoutExpired:
        process.kill()
        process.communicate()

    read_cmd = ["tcpdump", "-r", output_file]
    output_text_file = os.path.join(os.getcwd(), "tcpdump_output.txt")

    # remove output txt file if it already exits
    if os.path.exists(output_text_file):
        os.remove(output_text_file)

    with open(output_text_file, "w") as f:
        subprocess.run(read_cmd, stdout=f)

    with open(output_text_file, "r") as f:
        for line in f:
            process_line(line, local_ip, link_energy, node_name)
    for ip, energy in link_energy.items():
        for node, node_ip in link_address.items():
            if node_ip ==ip:
                dst_node = node
        write_to_file(filename, f"link.energy.consumption.from.{node_name}.to.{dst_node}.during.{duration_minutes}={energy:.2f}.microwatts\n")
    return link_energy

def measure_latency(nodes, iperfTime, node_name):
    for n1 in nodes:
        if n1 != node_name :
            pod_name = nodes[node_name]['HostNetPerf']
            target_ip = nodes[n1]['IPaddress']
            netperf_RR = run_netperf(pod_name, target_ip, iperfTime, 'TCP_RR', 'P50_LATENCY,P90_LATENCY,P99_LATENCY,THROUGHPUT,THROUGHPUT_UNITS')
            print(f"InterNode NetPerf TCP_RR test between Node {node_name} and Node {n1}: {netperf_RR}")
            # Splitting the result into four parts
            sc1, sc2, sc3, sc4, sc5 = netperf_RR.split(',') if netperf_RR else (None, None, None, None)
            write_to_file(filename, f"netperf.p90.latency.milliseconds.origin.{node_name}.destination.{n1}={sc2}\n")

def measure_bandwidth_and_packet_loss(nodes, iperfTime, node_name):
    for n1 in nodes:
        if n1 != node_name :
            pod_name = nodes[node_name]['HostNetPerf']
            target_ip = nodes[n1]['IPaddress']
            bw = run_iperf(pod_name, target_ip, iperfTime)
            print(f"InterNode iPerf Bandwidth test between Node {node_name} and Node {n1}: {bw}")
            # Splitting the result into four parts
            write_to_file(filename, f"iPerf.bw.Gbits.Sec.origin.{node_name}.destination.{n1}={bw}\n")
            packet_loss = run_iperf_udp(pod_name, target_ip, iperfTime)
            print(f"InterNode iPerf packet_loss test between Node {node_name} and Node {n1}: {packet_loss}")
            # Splitting the result into four parts
            write_to_file(filename, f"iPerf.loss.origin.{node_name}.destination.{n1}={packet_loss}\n")

async def main():
    local_ip = get_local_ip()
    node_name = get_node_name(local_ip)
    duration_minutes_link_failure = 1
    duration_minutes_link_energy = 2
    print(f"local ip is : {local_ip}")

    if os.path.exists(filename):
        os.remove(filename)

    nodes, netperf_pods, netperf_services = get_kubernetes_info()

    tasks = [
        measure_link_failures(local_ip, node_name, duration_minutes_link_failure),
        measure_link_energy(local_ip, node_name, duration_minutes_link_energy, interface),
    ]

    await asyncio.gather(*tasks)

    ####################### node degree #####################
    measure_node_degree(local_ip,node_name)

    # ####################### link failures #####################
    # await measure_link_failures(local_ip, node_name, duration_minutes)

    # ####################### link energy ########################
    # await measure_link_energy(local_ip, node_name, duration_minutes, interface)
    ################################ latency #################################
    # run netperf for latency
    measure_latency(nodes, iperfTime, node_name)
    ################################ bandwidth & packet loss #################################
    # run iperf for bandwidth and packet loss
    measure_bandwidth_and_packet_loss(nodes, iperfTime, node_name)
    ################################ create configmap #################################
    configmap_name = "netperf-metrics"
    namespace = "default"  
    file_path = "netperfMetrics.txt"  

    create_or_update_configmap(configmap_name, namespace, file_path)

if __name__ == "__main__":
    asyncio.run(main())
