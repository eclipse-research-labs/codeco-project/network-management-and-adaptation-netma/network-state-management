**Table of contents**

[TOC]

# 1. HE-CODECO NetMA sub-component NSM-Monitoring

NSM-Monitoring is a sub-component of the HE-CODECO NetMA that focuses on monitoring network data using a network performance probe. The probe has been based on the existing open-source code of the [Kubernetes netperf](https://github.com/leannetworking/k8s-netperf) licensed under Apache 2.0.

This specific adaptation provides the following: the probe is designed to measure various network metrics, including bandwidth, throughput, latency, packet loss rate, jitter, retransmission rate, and network interface statistics. 

Its usage scenarios include: 

- Real-time Network Monitoring: Continuously monitoring network metrics. 
- On-Demand Network Measurement: Initiating network performance measurements based on external triggers or user requests.

![NetMa_architecture](./NetMa_architecture.png)

# 2. Quick Start

Steps for getting a demo to run on a local Linux environment and a local KiND cluster.

## 2.1 Preconditions

- Docker
- Kubectl
- A cluster with no less than 2 nodes

## 2.2 Copy required scripts to local environment

- One way is to clone the nsm-mon repository

```
cd <nsm-mon-directory>
git clone http.....
```

## 2.3 Set up a Kubernetes cluster

Ensure that you have a K8s cluster with no less than 2 nodes running. In this section, we provide examples for the tools **KinD** and **Minikube**.

### 2.3.1 With KinD

1. Create kind config file:

```plaintext
cat > kind-config.yaml <<EOF
# four node (three workers) cluster config
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
- role: worker
- role: worker
- role: worker
EOF
```

2 . Create cluster:

```plaintext
kind create cluster --name <cluster_name> --config kind-config.yaml
```

To delete the cluster:

```plaintext
kind delete cluster --name <cluster_name>
```



### 2.3.2 With minikube

Set up a cluster with (x) number of nodes under the profile codeco:

```plaintext
minikube start --nodes x -p <cluster-name> 
```

To delete the cluster:

```plaintext
minikube delete -- all 
```



## 2.4 Test

### 2.4.1 Removing Taints from Master Nodes

**Viewing All Nodes and Their Taints**: You can get an overview of all nodes using the command `kubectl get nodes -o wide`. However, to view specific taint information, you need to use the command `kubectl describe node [node name]` or get more detailed output with the `-o json` parameter.

```python
kubectl get node [node name] -o json | jq '.spec.taints'
# or
kubectl describe node [node name]
```

To remove a taint from a node, you can use the `kubectl taint` command. The basic syntax for removing a taint is as follows:

```bash
kubectl taint nodes <node-name> <taint-key>-
```

An example：

```bash
kubectl taint nodes my-cluster-control-plane node-role.kubernetes.io/control-plane-
```

### 2.4.2 Deploying netperf as Daemon Pods

The netperf component, based on the [k8s-netperf](https://github.com/leannetworking/k8s-netperf) open-source project, has been enhanced in our Dockerfile to ensure compatibility across both arm64 and amd64 architectures.

To deploy, follow these steps:

```
cd k8s-netperf
kubectl apply -f k8s-netperf.yaml
```

### 2.4.3 Setting Up Periodic Network Monitoring

For regular network monitoring, we configure a CronJob to update every 5 minutes. You can apply this setting by executing the following commands:

```
cd ../Monitoring
kubectl apply -f serviceaccount.yaml
kubectl apply -f cronJob.yaml
```

### 2.4.4 Verifying the Results

The CronJob will be created every 5 minutes (at times ending with 0/5). You can check the CronJob with the following command, and you should see output similar to the following:

```
user:~/Desktop/NSM_Mon/Monitoring$ kubectl get job
NAME                  COMPLETIONS   DURATION   AGE
my-cronjob-28504140   1/1           4m49s      5m26s
my-cronjob-28504145   0/1           26s        26s
```

Each CronJob will spawn a corresponding pod to perform network measurements and store the results as a configmap:

```
user:~/Desktop/NSM_Mon/Monitoring$ kubectl get po
NAME                           READY   STATUS      RESTARTS   AGE
my-cronjob-28504140-lrf68      0/1     Completed   0          5m7s
my-cronjob-28504145-kcm44      1/1     Running     0          7s
netperf-host-6q9hq             1/1     Running     0          6m10s
netperf-host-9g6mt             1/1     Running     0          6m10s
netperf-host-rmj5m             1/1     Running     0          113s
netperf-host-trd8r             1/1     Running     0          6m10s
netperf-pod-5ztpm              1/1     Running     0          6m10s
netperf-pod-76cf587d6f-rr5s2   1/1     Running     0          6m10s
netperf-pod-76cf587d6f-rtpv9   1/1     Running     0          6m10s
netperf-pod-grpq4              1/1     Running     0          6m10s
netperf-pod-nhqqh              1/1     Running     0          6m10s
netperf-pod-s5j7n              1/1     Running     0          113s
```

When a CronJob is in the Completed status, it indicates that a test has finished. You can check the configmap to retrieve the network monitoring results:

```
user:~/Desktop/NSM_Mon/Monitoring$ kubectl get configmap
NAME               DATA   AGE
kube-root-ca.crt   1      16m
netperf-metrics    24     6s

```
# 3. Licensing
Apache 2.0

# 4. Contributing
Create your own branch and start updates - request merges when necessary

# Credits
Port to python and adaptations to HE-CODECO NetMA are being developed by fortiss, Hongyu Guo.

